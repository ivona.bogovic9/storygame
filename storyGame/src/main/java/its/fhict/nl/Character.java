package its.fhict.nl;

public class Character {
    private String name;
    private Gender gender;
    private Species species;

    public Character(String name, Gender gender, Species species) {
        this.name = name;
        this.gender = gender;
        this.species = species;
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", species=" + species +
                '}';
    }
}
